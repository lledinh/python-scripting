# Python scripting

Exercice strextract

Ecrire un programme Python 3 qui parcourt récursivement tous les fichiers d’une hiérarchie et produit en sortie toutes les chaînes littérales (= les textes entourés par " ou ’) que contiennent ces fichiers.

Les chaînes seront affichées sous la forme où elles étaient dans le fichier, entourées des même symboles " ou ’.

Exercice brklink

Ecrire une commande brklnk en Python 3 qui parcourt tous les liens contenus dans une page web et affiche les liens cassés.

Exercice ciqual

Écrire un programme Python3 qui importe dans une base de données Postgres ou Sqlite3 les données de nutrition de Ciqual