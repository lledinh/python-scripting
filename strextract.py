#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import argparse
import re

'''
Function which recursively traverse files and folders and output the strings 
surrounded by quotes ("" or '') which are present in the files

directory_path:     root folder where the search start 
suffix:             search files ending with suffix
show_path,          show file path before each result
accept_hidden_files read hidden files
'''
def quote_extract(directory_path, suffix, show_path, accept_hidden_files):
    regex_quotes = "\".+?\"|'.+?'"
    pattern_quotes = re.compile(regex_quotes)

    for root, _, filenames in os.walk(directory_path):
        for filename in filenames:
            # Read all the files if suffix isn't defined or read files ending with supplied suffix
            if not suffix or filename.endswith(suffix):
                # Read files which aren't hidden or read all files including hidden files if the parameter accept_hidden_files is true
                if not filename.startswith(".") or accept_hidden_files:
                    fullpath = os.path.join(root, filename)
                    try:
                        with open(fullpath, 'r') as file:
                            for line in file:
                                for match in pattern_quotes.finditer(line):
                                    if show_path:
                                        print ("{0}\t {1}".format(fullpath, match.group(0)))
                                    else:
                                        print ("{}".format(match.group(0)))

                                '''if matches is not None:
                                    for m in matches:
                                        if show_path:
                                            print ("{0}\t {1}".format(fullpath, m.group(0)))
                                        else:
                                            print ("{}".format(m.group(0)))'''
                    except Exception as e:
                        print ("Could not read file {}".format(filename))


'''
Main CLI program
–suffix=suffix : limit the search to the files ending with suffix
–path: Each line is preceded with the path of the associated file
-a, –all: include hidden files
'''
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("d", type=str, help="The directory containing the files.")
    parser.add_argument("-s", "--suffix", type=str, default="", help="Limit the search to the files ending with the suffix.")
    parser.add_argument("-p", "--path", type=bool, nargs="?", const=True, default=False, help="Each line is preceded with the path of the file.")
    parser.add_argument("-a", "--all", type=bool, nargs="?", const=True, default=False, help="Include hidden files.")
    args = parser.parse_args()
    
    quote_extract(args.d, args.suffix, args.path, args.all)

if __name__ == '__main__':
    main()