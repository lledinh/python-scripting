#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import argparse
from bs4 import BeautifulSoup
import urllib
import re
from urllib import parse

HTTP_ERROR = 400

def brklnk(url, root_url, depth, visited_links, verbose):
    if depth >= 0:
        # I have to keep this line to correctly format relative url, ie page2.html
        url = urllib.parse.urljoin(root_url, url)
        response = requests.get(url)
            
        if response.status_code >= HTTP_ERROR:
            print(url)
        else:
            if verbose:
                print("Status code {}".format(response.status_code))

            visited_links.append(url)
            soup = BeautifulSoup(response.text, 'html.parser')
            for a in soup.find_all('a', href = True):
                link = a['href']
                # Check if the link wasn't already visited
                if link not in visited_links:
                    if verbose:
                        print("Processing url {}".format(link))
                        
                    brklnk(link, url, depth - 1, visited_links, verbose)
               


'''
Main program CLI
--depth: depth search
--verbose: verbose mode
'''
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("url", type=str, help="The url of the website.")
    parser.add_argument("-d", "--depth", type=int, default=1, help="Limit search to the chosen depth.")
    parser.add_argument("-v", "--verbose", type=bool, nargs="?", const=True, default=False, help="Verbose mode.")
    args = parser.parse_args()
    brklnk(args.url, args.url, args.depth, [], args.verbose)

if __name__ == '__main__':
    main()
