CREATE TABLE Nutrient (
   id integer primary key autoincrement,
   name text
);

CREATE TABLE Food (
   id text,
   name text,
   grp_id text,
   ssgrp_id text,
   ssssgrp_id text
);

CREATE TABLE FoodGroup (
   id text,
   name text,
   father_grp_id text
);

CREATE TABLE NutData (
   id integer primary key autoincrement,
   food_id text,
   nutrient_id integer,
   value text
);